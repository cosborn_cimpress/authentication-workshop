# Extracting GitLab Project Name Examples

`https://gitlab.com/Cimpress-Technology/group/repository`
: Cimpress-Technology/group/repository

`https://gitlab.com/vistaprint-org/group/repository`
: vistaprint-org/group/repository

`https://gitlab.com/Cimpress-Technology/group/subgroup/repository`
: Cimpress-Technology/group/subgroup/repository

`https://gitlab.com/Cimpress-Technology/group/subgroup/sandbox/demonstrations/authentication/starlight/starbright/first/star/i/see/tonight/repository`
: Cimpress-Technology/group/subgroup/sandbox/demonstrations/authentication/starlight/starbright/first/star/i/see/tonight/repository
