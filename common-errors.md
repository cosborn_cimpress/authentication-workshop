## Common Errors

> Resource handler returned message: "Provider with url https://gitlab.com already exists. (Service: Iam, Status Code: 409, Request ID: \<GUID\>)"

There is already an AWS::IAM::OIDCProvider in your account for "https://gitlab.com". You should already be able to perform GitLab authentication to AWS. If you'd like to continue with the workshop, remove the resource `GitLabIdentityProvider` from the template and deploy only the role. Alternatively, check pipelines in projects you own to see the authentication happening.

It may also be the case that two participants in the workshop are trying to use the same AWS account for the workshop. Please consult among yourselves.

> An error occurred (ValidationError) when calling the CreateChangeSet operation: Parameter 'Project' must contain at least 1 characters

The deployment scripts expect an environment variable called `PROJECT` (if Bash) or an argument called `project` (if Powershell) containing the name (including path!) of the project you're using for this workshop.

> An error occurred (AccessDenied) when calling the AssumeRoleWithWebIdentity operation: Not authorized to perform sts:AssumeRoleWithWebIdentity

This is probably due to an error specifying the project into the IAM Role. Check that you have determined that correctly, consulting `project.md` in this repository for examples.

It may also be the case that two participants in the workshop are trying to use the same AWS account for the workshop. A later deployment overwrote an earlier one. Please consult among yourselves.