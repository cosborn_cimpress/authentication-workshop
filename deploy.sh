#!/usr/bin/env sh

aws cloudformation deploy \
  --stack-name 'gitlab-authentication-demo' \
  --template-file ./template.yml \
  --parameter-overrides "Project=${PROJECT}" \
  --tags 'Purpose=Demonstration' \
  --no-fail-on-empty-changeset \
  --capabilities CAPABILITY_IAM &&
aws cloudformation describe-stacks \
  --stack-name 'gitlab-authentication-demo' \
  --query "Stacks[0].Outputs[?OutputKey=='DemonstrationRole'].OutputValue" \
  --output text
