## GitLab Authentication Workshop Artifacts

This repository contains documents referred to or used in the GitLab CI/CD Authentication Workshop.

Detailed instructions are available [on Confluence](https://cimpress-support.atlassian.net/wiki/spaces/FD/pages/15605432386/OIDC+Authentication+from+GitLab+to+AWS).
